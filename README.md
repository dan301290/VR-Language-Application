#	VR Language Learning Application
This project was undertaken as a dissertation project for a MSc in Software Engineering at Leeds Beckett University.

##	Aim and objectives
The overall aim of the project was to develop a prototype VR(Virtual Reality) application that could potentially teach a user a new language or improve their speaking ability.
This would be done by replicating an everyday conversation a user may have in a foreign language, in a virtual environment using speech recognition technology to provide feedback on the users speaking ability i.e.
	- Interacting with a waiter/waitress in a café.
	- Buying a train ticket.
	
##	Rationale and background
Background research which looked at the results of several studies into the benefits of time spent abroad by language students, showed “A majority of students make measurable progress in speaking, especially in terms of fluency”. However, the cost of spending time in foreign country can be expensive (Cost of living, rent etc.), the prototype tool that was developed aims to replicate some of the benefits of spending time abroad without the cost.

##	Implementation
The Prototype VR application:

 - Developed for the Samsung GearVR platform using the Unity Game Engine (GearVR applications run on android)
 - Libraries developed by [Takohi](http://www.takohi.com/unity-assets/) provided simpler access to androids built in Speech Recognition and Text-to-Speech services.
	 - [Unity Android TTS Plugin](http://www.takohi.com/data/unity/assets/android_tts/documentation/d1/d6e/class_t_t_s_manager.html)
	 - [Android Speech Recognizer Plugin](http://www.takohi.com/data/unity/assets/android_speech_recognizer/documentation/class_speech_recognizer_manager.html)
 - C# was the main language used for writing the scripts that handled game-play/interaction with androids built in Speech Recognition and Text-to-Speech services via Takohi libraries (which themselves are written in Java
	
##	Result
The prototype VR application can do the following:

 - replicate a single scenario (interacting with a waitress at a restaurant) in a virtual environment, in English French and German.
 - provide feedback on a users speaking ability as the conversation takes place by making use of androids speech recognition service
 - enable a user to hear how words/phrases should be pronounced by making use of androids TTS service.
	
##	Gameplay/Images
#### Screen 1 - Main Menu
![Main Menu](https://gitlab.com/dan301290/VR-Language-Application/raw/master/images/mainMenu.png)
Shows the main menu of the application, which enables:

 - The user to select, the user interface language, the language they would like to learn/improve and lastly the lesson/scenario.
 - Gameplay instructions are displayed at the top of the screen.

#### Screen 2 - Question Menu
![Question Menu](https://gitlab.com/dan301290/VR-Language-Application/raw/master/images/questionMenu.png)
Shows the following:

 - The question that the user has been asked by the "waiter/waitress", highlighting the text will result in the waitress repeating the question.
 - Three possible correct answers, the user needs to select the correct one, highlighting each will allow the user to hear how they should be spoken.
 - A button to submit the chosen answer.
	
#### Screen 3 - Speech Menu
![Speech Menu](https://gitlab.com/dan301290/VR-Language-Application/raw/master/images/speechMenu.png)
Shows the following: 

 - The correct chosen response, highlighting the text will allow the user to hear how it should be spoken.
 - A record button, upon highlighting this button, the user will be prompted to speak the correct answer and be given feedback on the accuracy of their response.
 - A button that moves the scenario on to the next question/part of the conversation.

## Notes
*the full source code/assets used in the implementation of this project are not included in this repository. This repository exists as part of a portfolio and therefore only includes the relevant files for the core features of the application.