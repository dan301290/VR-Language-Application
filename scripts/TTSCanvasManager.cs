﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TTSCanvasManager : MonoBehaviour {
    // Vars
    private bool _initializeError = false;
    private string _inputText = "\n\n\n\n";
    private int _speechId = 0;
    private float _pitch = 1f, _speechRate = 1f;
    private int _selectedLocale = 0;
    private string[] _localeStrings;

    public SkinnedMeshRenderer mesh;
    public SpeechCanvasManager SpeechManager;
    public Slider slider;

    // Inlisise canvas
    // Load Canvas
    public Canvas Center, Left, Right;
    // Speak Button
    public Button SpeakBtnA;
    public Text SpeakBtnTextA;
    public Button SpeakBtnB;
    public Text SpeakBtnTextB;
    public Button SpeakBtnC;
    public Text SpeakBtnTextC;
    // User Selected
    public Button UserSelectedBtn;
    public Text UserSelectedBtnText;
    // AI Speech
    public Button UserSpeakBtn;
    public Text UserSpeakBtnText;
    // User Selected Phrase
    public Button UserSelectedPhrase;
    public Text UserSelectedPhraseText;
    // UI
    public Text QuestionCount;

    public Questions[] QuestionsSelected;
    public Questions a;
    public int qCount;
    public string lang;

    // Quit Button
    public Button QuitBtn;
    public Text QuitBtnText;
    public Text MenuMessage;

    // Timer
    public float GazeStartTime;
    public float GazeEndTime;
    public float sliderValue;

    public UILang selectedLang;
    public System.Random rnd = new System.Random();

    // define button images for manipulation
    private Image speakBgA, speakBgB, speakBgC, speakBgSelected, speakBgSpeak;

    public TTSManager.Locale locale;


    // Use this for initialization
    void Start() {

        TTSManager.Initialize(transform.name, "OnTTSInit");
        selectedLang = new UILang().getEngUILangData();

        // Repeater Methods
        InvokeRepeating("eyesShut", 0, 6.0f);
        InvokeRepeating("eyesOpen", 6.1f, 6.0f);

        speakBgA = SpeakBtnA.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        speakBgB = SpeakBtnB.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        speakBgC = SpeakBtnC.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();

        UserSelectedBtnText.text = selectedLang.getresultSubmit() + "\n" + SpeakBtnTextA.text;
        UserSelectedPhraseText.text = selectedLang.getrepeatResult() + "\n" + SpeakBtnTextA.text;
        speakBgA.color = Color.green;
        speakBgB.color = Color.red;
        speakBgC.color = Color.red;

    }

    public void eyesShut()
    {
        mesh.SetBlendShapeWeight(0, 100);
        mesh.SetBlendShapeWeight(1, 100);
    }

    public void eyesOpen()
    {
        mesh.SetBlendShapeWeight(0, 0);
        mesh.SetBlendShapeWeight(1, 0);
    }

    public void mouthOpen()
    {
        mesh.SetBlendShapeWeight(33, 100);
    }

    public void mouthshut()
    {
        mesh.SetBlendShapeWeight(33, 0);
    }

    // Update is called once per frame
    void Update() {

    }

    void OnDestroy()
    {
        TTSManager.Shutdown();
    }

    #region GUI
    // Read outload the selected result.
    public void OnGazeEnterA()
    {
        GazeStartTime = Time.time;
        slider = SpeakBtnA.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitA()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            TTSManager.Stop();
            speak();
            TTSManager.Speak(SpeakBtnTextA.text, false, TTSManager.STREAM.Music, 1f, 0f, transform.name, "OnSpeechCompleted", "speech_" + (++_speechId));
            UserSelectedBtnText.text = selectedLang.getresultSubmit() + "\n" + SpeakBtnTextA.text;
            UserSelectedPhraseText.text = selectedLang.getrepeatResult() + "\n" + SpeakBtnTextA.text;

            speakBgA.color = Color.green;
            speakBgB.color = Color.red;
            speakBgC.color = Color.red;
        }
    }

    public void OnGazeEnterB()
    {
        GazeStartTime = Time.time;
        slider = SpeakBtnB.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitB()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            TTSManager.Stop();
            speak();
            TTSManager.Speak(SpeakBtnTextB.text, false, TTSManager.STREAM.Music, 1f, 0f, transform.name, "OnSpeechCompleted", "speech_" + (++_speechId));
            UserSelectedBtnText.text = selectedLang.getresultSubmit() + "\n" + SpeakBtnTextB.text;
            UserSelectedPhraseText.text = selectedLang.getrepeatResult() + "\n" + SpeakBtnTextB.text;

            speakBgA.color = Color.red;
            speakBgB.color = Color.green;
            speakBgC.color = Color.red;
        }
    }

    public void OnGazeEnterC()
    {
        GazeStartTime = Time.time;
        slider = SpeakBtnC.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitC()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            TTSManager.Stop();
            speak();
            TTSManager.Speak(SpeakBtnTextC.text, false, TTSManager.STREAM.Music, 1f, 0f, transform.name, "OnSpeechCompleted", "speech_" + (++_speechId));
            UserSelectedBtnText.text = selectedLang.getresultSubmit() + "\n" + SpeakBtnTextC.text;
            UserSelectedPhraseText.text = selectedLang.getrepeatResult() + "\n" + SpeakBtnTextC.text;

            speakBgA.color = Color.red;
            speakBgB.color = Color.red;
            speakBgC.color = Color.green;
        }
    }


    // Repeat what the user said
    public void OnGazeEnterUserSpeak()
    {
        GazeStartTime = Time.time;
        slider = UserSpeakBtn.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitUserSpeak()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            speak();
            TTSManager.Speak(UserSpeakBtnText.text.Replace(selectedLang.getrepeatQuestion(), ""), false, TTSManager.STREAM.Music, 1f, 0f, transform.name, "OnSpeechCompleted", "speech_" + (++_speechId));
        }

    }

    public void SpeakNext()
    {
        speak();
        TTSManager.Speak(UserSpeakBtnText.text.Replace(selectedLang.getrepeatQuestion(), ""), false, TTSManager.STREAM.Music, 1f, 0f, transform.name, "OnSpeechCompleted", "speech_" + (++_speechId));
    }

    public void OnGazeEnterSelectedSpeak()
    {
        GazeStartTime = Time.time;
        slider = UserSelectedPhrase.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitSelectedSpeak()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            speak();
            TTSManager.Speak(UserSelectedPhraseText.text.Replace(selectedLang.getrepeatResult(), ""), false, TTSManager.STREAM.Music, 1f, 0f, transform.name, "OnSpeechCompleted", "speech_" + (++_speechId));
        }
    }

    public void OnGazeExit()
    {
        //TTSManager.Stop();

    }

    // Awnser Selected
    public void OnGazeEnterUserSelected()
    {
        GazeStartTime = Time.time;
        slider = UserSelectedBtn.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    // Check Correct Anwser Selected
    // -------------------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------------
    public void OnGazeExitUserSelected()
    {

        GazeEndTime = Time.time;
        SliderExit();
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            if (UserSelectedBtnText.text.Contains(QuestionsSelected[qCount].getCorrectResult()))
            {
                qCount++;
                if ((qCount + 1) >= QuestionsSelected.Length)
                {
                    SpeechManager.finalQuestion = true;
                }

                QuestionCount.text = "Q: " + (qCount + 1) + "/" + QuestionsSelected.Length;
                UserSpeakBtnText.text = selectedLang.getrepeatQuestion() + "\n" + QuestionsSelected[qCount].getQuestion();

                int caseSwitch = rnd.Next(1, 4);

                switch (caseSwitch)
                {
                    case 1:
                        SpeakBtnTextA.text = QuestionsSelected[qCount].getResultA();
                        SpeakBtnTextB.text = QuestionsSelected[qCount].getResultB();
                        SpeakBtnTextC.text = QuestionsSelected[qCount].getResultC();
                        break;
                    case 2:
                        SpeakBtnTextA.text = QuestionsSelected[qCount].getResultB();
                        SpeakBtnTextB.text = QuestionsSelected[qCount].getResultA();
                        SpeakBtnTextC.text = QuestionsSelected[qCount].getResultC();
                        break;
                    case 3:
                        SpeakBtnTextA.text = QuestionsSelected[qCount].getResultC();
                        SpeakBtnTextB.text = QuestionsSelected[qCount].getResultB();
                        SpeakBtnTextC.text = QuestionsSelected[qCount].getResultA();
                        break;
                    default:
                        SpeakBtnTextA.text = QuestionsSelected[qCount].getResultA();
                        SpeakBtnTextB.text = QuestionsSelected[qCount].getResultB();
                        SpeakBtnTextC.text = QuestionsSelected[qCount].getResultC();
                        break;
                }

                // Hide Question Menu and show speak Menu
                Left.enabled = true;
                Right.enabled = false;
                Center.enabled = false;
                MenuMessage.text = selectedLang.getinstruction2();


            } else
            {
                speak();
                TTSManager.SetLanguage(selectedLang.getLocale());
                TTSManager.Speak(selectedLang.getTryAgain(), false, TTSManager.STREAM.Music, 1f, 0f, transform.name, "OnSpeechCompleted", "speech_" + (++_speechId));
                TTSManager.SetLanguage(locale);
            }
        }
    }

    public void updateQuestion()
    {
        // Update Selected Question
        UserSelectedBtnText.text = selectedLang.getresultSubmit() + "\n" + SpeakBtnTextA.text;
        UserSelectedPhraseText.text = selectedLang.getrepeatResult() + "\n" + SpeakBtnTextA.text;
        speakBgA.color = Color.green;
        speakBgB.color = Color.red;
        speakBgC.color = Color.red;
    }

    public void OnClick()
    {
        
    }

    public void UpdateQuestions(Questions[] questions, TTSManager.Locale locale)
    {
        qCount = 0;
        QuestionsSelected = questions;
        TTSManager.SetLanguage(locale);
        this.locale = locale;
        QuestionCount.text = "Q: " + (qCount+1) + "/" + QuestionsSelected.Length;

        UserSpeakBtnText.text = selectedLang.getrepeatQuestion() + "\n" + QuestionsSelected[qCount].getQuestion();

        int caseSwitch = rnd.Next(1,4);

        switch (caseSwitch)
        {
            case 1:
                SpeakBtnTextA.text = QuestionsSelected[qCount].getResultA();
                SpeakBtnTextB.text = QuestionsSelected[qCount].getResultB();
                SpeakBtnTextC.text = QuestionsSelected[qCount].getResultC();
                break;
            case 2:
                SpeakBtnTextA.text = QuestionsSelected[qCount].getResultB();
                SpeakBtnTextB.text = QuestionsSelected[qCount].getResultA();
                SpeakBtnTextC.text = QuestionsSelected[qCount].getResultC();
                break;
            case 3:
                SpeakBtnTextA.text = QuestionsSelected[qCount].getResultC();
                SpeakBtnTextB.text = QuestionsSelected[qCount].getResultB();
                SpeakBtnTextC.text = QuestionsSelected[qCount].getResultA();
                break;
            default:
                SpeakBtnTextA.text = QuestionsSelected[qCount].getResultA();
                SpeakBtnTextB.text = QuestionsSelected[qCount].getResultB();
                SpeakBtnTextC.text = QuestionsSelected[qCount].getResultC();
                break;
        }

        speakBgA.color = Color.green;
        speakBgB.color = Color.red;
        speakBgC.color = Color.red;

        // Update Selected Question
        UserSelectedBtnText.text = selectedLang.getresultSubmit() + "\n" + SpeakBtnTextA.text;
        UserSelectedPhraseText.text = selectedLang.getrepeatResult() + "\n" + SpeakBtnTextA.text;

        speak();
        TTSManager.Speak(UserSpeakBtnText.text.Replace(selectedLang.getrepeatQuestion(), ""), false, TTSManager.STREAM.Music, 1f, 0f, transform.name, "OnSpeechCompleted", "speech_" + (++_speechId));
    }



    #endregion

    void OnTTSInit(string message)
    {
        int response = int.Parse(message);

        switch (response)
        {
            case TTSManager.SUCCESS:
                List<TTSManager.Locale> l = TTSManager.GetAvailableLanguages();
                _localeStrings = new string[l.Count];
                for (int i = 0; i < _localeStrings.Length; ++i)
                    _localeStrings[i] = l[i].Name;

                break;
            case TTSManager.ERROR:
                _initializeError = true;
                break;
        }
    }
    // mouth animation
    public void speak()
    {
        InvokeRepeating("mouthOpen", 0, 0.3f);
        InvokeRepeating("mouthshut", 0, 0.4f);
    }

    // stop mouth animation
    void OnSpeechCompleted(string id)
    {

        CancelInvoke("mouthOpen");
        CancelInvoke("mouthshut");
        mesh.SetBlendShapeWeight(33, 0);

        Debug.Log("Speech '" + id + "' is complete.");
    }

    void OnSynthesizeCompleted(string id)
    {
        Debug.Log("Synthesize of speech '" + id + "' is complete.");
    }

    // Deal with Gaze Events
    public void SliderEnter()
    {
        sliderValue = sliderValue + 0.1f;
        slider.value = sliderValue;
    }

    public void SliderExit()
    {
        sliderValue = 0;
        slider.value = sliderValue;
        CancelInvoke("SliderEnter");
    }

    public void updateUI()
    {
        UserSelectedBtnText.text = selectedLang.getresultSubmit();
        
}

}
