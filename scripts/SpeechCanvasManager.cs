﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.Linq;

public class SpeechCanvasManager : MonoBehaviour {

    // Inililise vars
    private SpeechRecognizerManager _speechManager = null;
    private bool _isListening = false;
    private string _message = "";
    private string lang;

    public TTSCanvasManager TTSCManager;

    // Inlisise canvas
    // Load Canvas
    public Canvas Center, Left, Right, MainMenu;
    public Slider slider;
    // rec Button
    public Button RecBtn;
    public Text RecBtnText;
    // User Selected
    public Button UserSelectedBtn;
    public Text UserSelectedBtnText;
    // User Selected Phrase
    public Button UserSelectedPhrase;
    public Text UserSelectedPhraseText;
    // Next Button
    public Button NextBtn;
    public Text NextBtnText;

    // Quit Button
    public Button QuitBtn;
    public Text QuitBtnText;
    public Text MenuMessage;

    // Result Area
    public Text resultTxt;
    public UILang selectedLang;

    // Timer
    public float GazeStartTime;
    public float GazeEndTime;
    public float sliderValue;

    // define button images for manipulation
    private Image RecBtnBg, UserSelectedPhraseBg, NextButtonBg;
    public bool finalQuestion;


    #region Methods

    // Use this for initialization
    void Start () {
        // check if running on android
        if (Application.platform != RuntimePlatform.Android)
        {
            Debug.Log("Speech reognition is only available on android");
            resultTxt.text = "Speech reognition is only available on android";
            return;
        }
        // check if device is compatible
        if (!SpeechRecognizerManager.IsAvailable())
        {
            Debug.Log("Speech reognition is not available on this device");
            resultTxt.text = resultTxt + "Speech reognition is not available on this device";
            return;
        }

        else
        {
            resultTxt.text = "Speech available";
        }

        _speechManager = new SpeechRecognizerManager(gameObject.name);
        selectedLang = new UILang().getEngUILangData();

        RecBtnBg = RecBtn.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        UserSelectedPhraseBg = UserSelectedPhrase.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        NextButtonBg = NextBtn.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();

        RecBtnBg.color = Color.red;
        UserSelectedPhraseBg.color = Color.white;
        NextButtonBg.color = Color.red;

        finalQuestion = false;


    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void updateUI()
    {
        RecBtnText.text = selectedLang.getRecord();
        NextBtnText.text = selectedLang.getNextQuestion();
        QuitBtnText.text = selectedLang.getQuit();
    }
    // Used to close speech manager
    private void OnDestroy()
    {
        if (_speechManager != null)
            _speechManager.Release();
    }

    #endregion

    #region Events
    // Log Speech Events
    void OnSpeechEvent (string e)
    {
        switch (int.Parse (e))
        {
            case SpeechRecognizerManager.EVENT_SPEECH_READY:
                DebugLog("Ready for speech");
                break;
            case SpeechRecognizerManager.EVENT_SPEECH_BEGINNING:
                DebugLog("User started speaking");
                break;
            case SpeechRecognizerManager.EVENT_SPEECH_END:
                DebugLog("User stopped speaking");
                break;
        }
    }

    // Deal with speech results
    void OnSpeechResults (string results)
    {
        _isListening = false;
        RecBtnText.text = selectedLang.getRecord();

        // Need to parse results
        string[] texts = results.Split(new string[] { SpeechRecognizerManager.RESULT_SEPARATOR }, System.StringSplitOptions.None);

        ArrayList scores = new ArrayList();

        string finalResult, incorrectResults;
        int resultCount, correctResultCount;
        double score;
        bool isCorrect;

        resultCount = 0;
        correctResultCount = 0;
        score = 0;
        finalResult = "";
        incorrectResults = selectedLang.getSpeechResults() + "\n";
        isCorrect = false;
        // used to set display

        foreach (string result in texts)
        {
            resultCount++;

            string valueReturned = Regex.Replace(result, @"[^\w\d\s]", "");
            string valueReturnedUnFiltered = result;
            string correctResult = UserSelectedPhraseText.text.Replace(selectedLang.getrepeatResult(), "");
            correctResult = correctResult.Replace("\n", "");
            correctResult = Regex.Replace(correctResult, @"[^\w\d\s]", "");

            if (valueReturned.ToLower().Contains(correctResult.ToLower()))
            {
                isCorrect = true;
                // add score for this sentence to scores
                scores.Add(Compare(valueReturned.ToLower(), correctResult.ToLower()));

                finalResult = selectedLang.getSpeechResults() + "\n   " + UserSelectedPhraseText.text.Replace(selectedLang.getrepeatResult(), "");
                correctResultCount = resultCount;
                // break out of foreach loop when correct result is matched.
                break;
            } else
            {
                // add score for this sentence to scores
                scores.Add(Compare(valueReturned.ToLower(), correctResult.ToLower()));

                incorrectResults = incorrectResults + valueReturnedUnFiltered + "\n";
            }
        }

        if (isCorrect == true)
        {
            UserSelectedPhraseBg.color = Color.green;

            // calculate score by adding up the total acuracy for each string resturnd and getting an average
            foreach (double result in scores)
            {
                score = score + result;
            }
            score = score / scores.Count;

            resultTxt.text = finalResult + "\n" + System.Math.Round(score, 2) + "%";
        } else
        {
            UserSelectedPhraseBg.color = Color.red;
            score = 0;
            resultTxt.text = incorrectResults;
        }
        DebugLog(selectedLang.getSpeechResults() + "\n   " + string.Join("\n   ", texts));
    }

    // compares two strings and gets match percentage
    double Compare(string a, string b)
    {
        var aWords = a.Split(' ');
        var bWords = b.Split(' ');
        double matches = (double)aWords.Count(x => bWords.Contains(x));
        return (matches / (double)aWords.Count()) * 100;
    }


    // Deal with speech error
    void OnSpeechError (string error)
    {
        switch (int.Parse(error))
        {
            case SpeechRecognizerManager.ERROR_AUDIO:
                DebugLog("Error during recording the audio.");
                resultTxt.text = "Error during recording the audio.";
                UserSelectedPhraseBg.color = Color.red;
                RecBtnText.text = selectedLang.getRecord();
                break;
            case SpeechRecognizerManager.ERROR_CLIENT:
                DebugLog("Error on the client side.");
                resultTxt.text = "Error on the client side.";
                UserSelectedPhraseBg.color = Color.red;
                RecBtnText.text = selectedLang.getRecord();
                break;
            case SpeechRecognizerManager.ERROR_INSUFFICIENT_PERMISSIONS:
                DebugLog("Insufficient permissions. Do the RECORD_AUDIO and INTERNET permissions have been added to the manifest?");
                resultTxt.text = "Insufficient permissions. Do the RECORD_AUDIO and INTERNET permissions have been added to the manifest?";
                UserSelectedPhraseBg.color = Color.red;
                RecBtnText.text = selectedLang.getRecord();
                break;
            case SpeechRecognizerManager.ERROR_NETWORK:
                DebugLog("A network error occured. Make sure the device has internet access.");
                resultTxt.text = "A network error occured. Make sure the device has internet access.";
                UserSelectedPhraseBg.color = Color.red;
                RecBtnText.text = selectedLang.getRecord();
                break;
            case SpeechRecognizerManager.ERROR_NETWORK_TIMEOUT:
                DebugLog("A network timeout occured. Make sure the device has internet access.");
                resultTxt.text = "A network timeout occured. Make sure the device has internet access.";
                UserSelectedPhraseBg.color = Color.red;
                RecBtnText.text = selectedLang.getRecord();
                break;
            case SpeechRecognizerManager.ERROR_NO_MATCH:
                DebugLog("No recognition result matched.");
                resultTxt.text = "No recognition result matched.";
                UserSelectedPhraseBg.color = Color.red;
                RecBtnText.text = selectedLang.getRecord();
                break;
            case SpeechRecognizerManager.ERROR_NOT_INITIALIZED:
                DebugLog("Speech recognizer is not initialized.");
                resultTxt.text = "Speech recognizer is not initialized.";
                UserSelectedPhraseBg.color = Color.red;
                RecBtnText.text = selectedLang.getRecord();
                break;
            case SpeechRecognizerManager.ERROR_RECOGNIZER_BUSY:
                DebugLog("Speech recognizer service is busy.");
                resultTxt.text = "Speech recognizer service is busy.";
                UserSelectedPhraseBg.color = Color.red;
                RecBtnText.text = selectedLang.getRecord();
                break;
            case SpeechRecognizerManager.ERROR_SERVER:
                DebugLog("Server sends error status.");
                resultTxt.text = "Server sends error status.";
                UserSelectedPhraseBg.color = Color.red;
                RecBtnText.text = selectedLang.getRecord();
                break;
            case SpeechRecognizerManager.ERROR_SPEECH_TIMEOUT:
                DebugLog("No speech input.");
                resultTxt.text = "No speech input.";
                UserSelectedPhraseBg.color = Color.red;
                RecBtnText.text = selectedLang.getRecord();
                break;
            default:
                break;
        }

        _isListening = false;
    }


    #endregion

    #region GUI

    public void OnGazeQuitEnter()
    {
        GazeStartTime = Time.time;
        slider = QuitBtn.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeQuitExit()
    {

        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            Left.enabled = false;
            Right.enabled = false;
            Center.enabled = true;
            MenuMessage.text = selectedLang.getQuitMessage();
            resultTxt.text = selectedLang.getSpeechResults();
            UserSelectedPhraseBg.color = Color.white;
        }
    }


    public void OnGazeEnter()
    {
        GazeStartTime = Time.time;
        slider = RecBtn.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExit()
    {

        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {

            if (_isListening == false)
            {
                RecBtnText.text = selectedLang.getListening();
                _speechManager.StartListening(3, lang);
            } else
            {
                RecBtnText.text = selectedLang.getRecord();
                _speechManager.StopListening();
                _isListening = false;
            }

        }

    }

    public void OnGazeEnterNext()
    {
        GazeStartTime = Time.time;
        slider = NextBtn.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitNext()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            if (finalQuestion == true)
            {
                // Return to main menu
                Left.enabled = false;
                Right.enabled = false;
                Center.enabled = true;
                finalQuestion = false;
                MenuMessage.text = selectedLang.getCompleteMessage();
            } else
            {
                // Hide speak Menu and show Question Menu
                Left.enabled = false;
                Right.enabled = true;
                Center.enabled = false;

                TTSCManager.updateQuestion();
                TTSCManager.SpeakNext();
                MenuMessage.text = selectedLang.getinstruction1();
            }
            // clear result area
            resultTxt.text = selectedLang.getSpeechResults();
            UserSelectedPhraseBg.color = Color.white;
        }
    }

    public void OnClick()
    {
        RecBtnText.text = "Gaze Click";
    }

    public void setLang(string a)
    {
        lang = a;
    }

    // Deal with Gaze Events
    public void SliderEnter()
    {
        sliderValue = sliderValue + 0.1f;
        slider.value = sliderValue;
    }

    public void SliderExit()
    {
        sliderValue = 0;
        slider.value = sliderValue;
        CancelInvoke("SliderEnter");
    }



    #endregion

    #region DEBUG

    private void DebugLog(string message)
    {
        Debug.Log(message);
        _message = message;
    }

    #endregion
}
