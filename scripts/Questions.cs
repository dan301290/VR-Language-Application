﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Questions : ScriptableObject
{
    // define vars
    public string Question;
    public string ResultA;
    public string ResultB;
    public string ResultC;
    public string CorrectResult;

    // define constructor

    public Questions()
    {

    }

    public Questions(string Question, string ResultA, string ResultB, string ResultC, string CorrectResult)
    {
        this.Question = Question;
        this.ResultA = ResultA;
        this.ResultB = ResultB;
        this.ResultC = ResultC;
        this.CorrectResult = CorrectResult;
    }


    public string getQuestion()
    {
        return Question;
    }

    public string getResultA()
    {
        return ResultA;
    }

    public string getResultB()
    {
        return ResultB;
    }

    public string getResultC()
    {
        return ResultC;
    }

    public string getCorrectResult()
    {
        return CorrectResult;
    }
    // English Coversation
    public Questions[] getEnglishQuestions()
    {
        Questions Question1 = new Questions("Hi, How are you this afternoon?", 
            "Fine, thank you. Can I see a menu, please?",
            "Fine, thank you. Can I see a food description, please?",
            "Fine, thank you. Can I see a cuisine, please?",
            "Fine, thank you. Can I see a menu, please?");
        Questions Question2 = new Questions("Certainly, here you are.", 
            "Thank you. What's today's special?",
            "Thank you. What's today's food?",
            "Thank you. What's today's date?",
            "Thank you. What's today's special?");
        Questions Question3 = new Questions("Grilled tuna and cheese on rye.",
            "That sounds good. I'll have that.",
            "That sounds good. I'll have them.",
            "That sounds good. I'll need that.",
            "That sounds good. I'll have that.");
        Questions Question4 = new Questions("Would you like something to drink?",
            "Yes, I'd like a glass of water.",
            "Yes, I'd like a gallon of water.",
            "Yes, I'd like a glacier of water.",
            "Yes, I'd like a glass of water.");
        Questions Question5 = new Questions("Here you are. Enjoy your meal!",
            "Thank you.",
            "No, thank you.",
            "Sorry.",
            "Thank you.");
        Questions Question6 = new Questions("Can I get you anything else?",
            "No thanks. I'd like the bill, please.",
            "No thanks. I'd like the money, please.",
            "No thanks. I'd like the paper, please.",
            "No thanks. I'd like the bill, please.");
        Questions Question7 = new Questions("That'll be six pounds, please",
            "Here you are. Keep the change!",
            "Here you are. Keep the money!",
            "Here you are. Keep the coins!",
            "Here you are. Keep the change!");
        Questions Question8 = new Questions("Thank you! Have a good day!",
            "Goodbye.",
            "Hello.",
            "Good.",
            "Goodbye.");

        Questions[] EngQuestions = new Questions[8];
        EngQuestions[0] = Question1;
        EngQuestions[1] = Question2;
        EngQuestions[2] = Question3;
        EngQuestions[3] = Question4;
        EngQuestions[4] = Question5;
        EngQuestions[5] = Question6;
        EngQuestions[6] = Question7;
        EngQuestions[7] = Question8;

        return EngQuestions;
    }
    // French Coversation
    public Questions[] getFrenchQuestions()
    {
        Questions Question1 = new Questions("Bonjour! Comment allez-vous aujourd’hui?",
            "Ça va bien, merci. La carte, s’il vous plaît.",
            "Ça va bien, merci. L’addition, s’il vous plaît.",
            "Ça va bien, merci. La nourriture, s’il vous plaît.",
            "Ça va bien, merci. La carte, s’il vous plaît.");
        Questions Question2 = new Questions("Bien sûr. Voilà.",
            "Merci. Quel est le plat du jour?",
            "Merci. C’est combien?",
            "Merci. Quelle est la date aujourd'hui?",
            "Merci. Quel est le plat du jour?");
        Questions Question3 = new Questions("Nous avons un filet de poisson grillé comme plat du jour.",
            "Très bien. Je le prends.",
            "Très bien. Je suis végétarien.",
            "Très bien. Je ne mange jamais de poisson.",
            "Très bien. Je le prends.");
        Questions Question4 = new Questions("Et pour boire?",
            "Je voudrais de l’eau, s’il vous plaît.",
            "Je voudrais des frites, s’il vous plaît.",
            "Je voudrais du vinaigre, s’il vous plaît.",
            "Je voudrais de l’eau, s’il vous plaît.");
        Questions Question5 = new Questions("Et voilà. Bon appétit!",
            "Merci.",
            "Pardon.",
            "Non.",
            "Merci.");
        Questions Question6 = new Questions("Vous prenez un dessert?",
            "Non, merci. L’addition, s’il vous plait.",
            "Non, merci. J’ai faim.",
            "Bien cuit, s’il vous plaît.",
            "Non, merci. L’addition, s’il vous plait.");
        Questions Question7 = new Questions("C’est 15 euros, s’il vous plaît",
            "Voilà. Gardez la monnaie!",
            "Ma commande n’est pas encore là",
            "Puis-je avoir du sel?",
            "Voilà. Gardez la monnaie!");
        Questions Question8 = new Questions("Merci. Au revoir!",
            "Au revoir.",
            "Bonjour.",
            "Bien.",
            "Au revoir.");


        Questions[] FreQuestions = new Questions[8];
        FreQuestions[0] = Question1;
        FreQuestions[1] = Question2;
        FreQuestions[2] = Question3;
        FreQuestions[3] = Question4;
        FreQuestions[4] = Question5;
        FreQuestions[5] = Question6;
        FreQuestions[6] = Question7;
        FreQuestions[7] = Question8;

        return FreQuestions;
    }
    // German Coversation
    public Questions[] getGermanQuestions()
    {
        Questions Question1 = new Questions("Hallo. Wie geht es Ihnen?",
            "Es geht mir gut, danke. Darf ich bitte die Karte sehen?",
            "Es geht mir gut, danke. Darf ich bitte die Zeitung sehen?",
            "Es geht mir gut, danke. Die Rechnung, bitte.",
            "Es geht mir gut, danke. Darf ich bitte die Karte sehen?");
        Questions Question2 = new Questions("Ja, bitte schön.",
            "Danke. Was ist das Angebot des Tages?",
            "Danke. Wie spät ist es?",
            "Danke. Welcher Tag ist heute?",
            "Danke. Was ist das Angebot des Tages?");
        Questions Question3 = new Questions("Thunfisch ist heute im Tagesangebot.",
            "Sehr gut. Das nehme ich.",
            "Einen Tisch für zwei, bitte.",
            "Ich esse kein Schweinefleisch.",
            "Sehr gut. Das nehme ich.");
        Questions Question4 = new Questions("Was möchten Sie trinken?",
            "Ich nehme ein Glas Wasser.",
            "Können wir Salz und Pfeffer bekommen?",
            "Ja. Ich hätte gern die Tagessuppe.",
            "Ich nehme ein Glas Wasser.");
        Questions Question5 = new Questions("Bitte sehr. Guten Appetit!",
            "Danke.",
            "Nein.",
            "Entschuldigung.",
            "Danke.");
        Questions Question6 = new Questions("Darf ich Ihnen noch etwas anderes bringen?",
            "Nein, danke. Die Rechnung, bitte.",
            "Nein, danke. Das Trinkgeld, bitte.",
            "Nein, danke. Ich bin durstig.",
            "Nein, danke. Die Rechnung, bitte.");
        Questions Question7 = new Questions("Das macht 15€, bitte.",
            "Bitte. Es stimmt so.",
            "Bitte. Die Speisekarte, bitte. ",
            "Bitte. Ich möchte eine Vorspeise.",
            "Bitte. Es stimmt so.");
        Questions Question8 = new Questions("Danke. Schönen Tag!",
            "Auf Wiedersehen!",
            "Hallo.",
            "Toll!",
            "Auf Wiedersehen!");


        Questions[] GerQuestions = new Questions[8];
        GerQuestions[0] = Question1;
        GerQuestions[1] = Question2;
        GerQuestions[2] = Question3;
        GerQuestions[3] = Question4;
        GerQuestions[4] = Question5;
        GerQuestions[5] = Question6;
        GerQuestions[6] = Question7;
        GerQuestions[7] = Question8;

        return GerQuestions;
    }


}
