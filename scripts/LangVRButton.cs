﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LangVRButton : MonoBehaviour {
    // Load Canvas
    public Canvas Center, Left, Right;

    // Load Scence Managers
    public SpeechCanvasManager SpeechManager;
    public TTSCanvasManager TTSCManager;

    public Slider slider;

    // Load UI Lang Buttons
    public Button UILangSelected, UILangA, UILangB, UILangC;
    public Text UILangSelectedText, UILangAText, UILangBText, UILangCText;

    // Load Lang Buttons
    public Button LangSelected, LangA, LangB, LangC;
    public Text LangSelectedText, LangAText, LangBText, LangCText;

    // Load Conv Buttons
    public Button ConvSelected, ConvA, ConvB, ConvC;
    public Text ConvSelectedText, ConvAText, ConvBText, ConvCText;

    // Load submit Button
    public Button Submit;
    public Text SubmitText;

    // Quit Button
    public Button QuitBtn;
    public Text QuitBtnText;
    public Text MenuMessage;

    // Timer
    public float GazeStartTime;
    public float GazeEndTime;
    public float sliderValue;

    // Lang/Lesson Strings
    public string UILang;
    // public string UILangIntro;
    public string SpRecLang;
    public string Lesson;

    // Questions
    public Questions[] QuestionsSelected;
    public Questions a;
    public TTSManager.Locale locale;
    public UILang selectedLang;
    // define button images for manipulation
    private Image UiBgA, UiBgB, UiBgC, BgA, BgB, BgC, ConBgA, ConBgB, ConBgC;

    // Use this for initialization - setup default selection as Lesson1 - English
    void Start () {
        Left.enabled = false;
        Right.enabled = false;

        slider = ConvB.GetComponentInChildren<Slider>();
        slider.enabled = false;
        slider.transform.localScale = new Vector3(0, 0, 0);
        ConvB.transform.localScale = new Vector3(0, 0, 0);
        ConvB.enabled = false;
        slider = ConvC.GetComponentInChildren<Slider>();
        slider.enabled = false;
        slider.transform.localScale = new Vector3(0, 0, 0);
        ConvC.transform.localScale = new Vector3(0, 0, 0);
        ConvC.enabled = false;

        selectedLang = new UILang().getEngUILangData();
        SpRecLang = "en-GB";

        UILangSelectedText.text = selectedLang.getinterfaceLang() + UILangAText.text;
        LangSelectedText.text = selectedLang.getlearnLang() + LangAText.text;
        ConvSelectedText.text = selectedLang.getlesson() + ConvAText.text;
        // default language setup
        SubmitText.text = selectedLang.gethomeSubmit();
        MenuMessage.text = selectedLang.getinstruction();

        a = new Questions();
        QuestionsSelected = a.getEnglishQuestions();
        locale = selectedLang.getLocale();

        QuitBtnText.text = selectedLang.getQuit();

        // setup images
        UiBgA = UILangA.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        UiBgB = UILangB.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        UiBgC = UILangC.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        BgA = LangA.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        BgB = LangB.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        BgC = LangC.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        ConBgA = ConvA.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        ConBgB = ConvB.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        ConBgC = ConvC.GetComponentInChildren<Slider>().GetComponentInChildren<Image>();
        // default selections
        UiBgA.color = Color.green;
        BgA.color = Color.green;
        ConBgA.color = Color.green;


    }
	
	// Update is called once per frame
	void Update () {
		
	}

    // Deal with UI language selection
    #region UIGAZE

    public void OnGazeEnterUIA()
    {
        GazeStartTime = Time.time;
        slider = UILangA.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitUIA()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            // set Selected language to English
            selectedLang = new UILang().getEngUILangData();

            // set selected button to Green  and update language
            UiBgA.color = Color.green;
            UiBgB.color = Color.red;
            UiBgC.color = Color.red;
            LangAText.text = "English";
            LangBText.text = "French";
            LangCText.text = "German";

            // set UI elements to english
            QuitBtnText.text = selectedLang.getQuit();
            MenuMessage.text = selectedLang.getinstruction();

            // set UI lang selected to English intro
            UILangSelectedText.text = selectedLang.getinterfaceLang() + UILangAText.text;

            UILang templang = new UILang();
            templang = templang.getFraUILangData();
            LangSelectedText.text = LangSelectedText.text.Replace(templang.getlearnLang(), selectedLang.getlearnLang());
            ConvSelectedText.text = ConvSelectedText.text.Replace(templang.getlesson(), selectedLang.getlesson());

            templang = templang.getGerUILangData();
            LangSelectedText.text = LangSelectedText.text.Replace(templang.getlearnLang(), selectedLang.getlearnLang());
            ConvSelectedText.text = ConvSelectedText.text.Replace(templang.getlesson(), selectedLang.getlesson());

            SubmitText.text = selectedLang.gethomeSubmit();
        }
    }

    public void OnGazeEnterUIB()
    {
        GazeStartTime = Time.time;
        slider = UILangB.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitUIB()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            // set Selected language to french
            selectedLang = new UILang().getFraUILangData();

            // set selected button to Green  and update language
            UiBgA.color = Color.red;
            UiBgB.color = Color.green;
            UiBgC.color = Color.red;
            LangAText.text = "Anglais";
            LangBText.text = "Français";
            LangCText.text = "Allemand";

            // set UI elements to french
            QuitBtnText.text = selectedLang.getQuit();
            MenuMessage.text = selectedLang.getinstruction();

            // set UI lang selected to french intro
            UILangSelectedText.text = selectedLang.getinterfaceLang() + UILangBText.text;

            UILang templang = new UILang();
            templang = templang.getEngUILangData();
            LangSelectedText.text = LangSelectedText.text.Replace(templang.getlearnLang(), selectedLang.getlearnLang());
            ConvSelectedText.text = ConvSelectedText.text.Replace(templang.getlesson(), selectedLang.getlesson());

            templang = templang.getGerUILangData();
            LangSelectedText.text = LangSelectedText.text.Replace(templang.getlearnLang(), selectedLang.getlearnLang());
            ConvSelectedText.text = ConvSelectedText.text.Replace(templang.getlesson(), selectedLang.getlesson());

            SubmitText.text = selectedLang.gethomeSubmit();
        }
    }

    public void OnGazeEnterUIC()
    {
        GazeStartTime = Time.time;
        slider = UILangC.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitUIC()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            // set Selected language to german
            selectedLang = new UILang().getGerUILangData();

            // set selected button to Green  and update language
            UiBgA.color = Color.red;
            UiBgB.color = Color.red;
            UiBgC.color = Color.green;
            LangAText.text = "Englisch";
            LangBText.text = "Französisch";
            LangCText.text = "Deutsche";

            // set UI elements to german
            QuitBtnText.text = selectedLang.getQuit();
            MenuMessage.text = selectedLang.getinstruction();

            // set UI lang selected to german intro
            UILangSelectedText.text = selectedLang.getinterfaceLang() + UILangCText.text;

            UILang templang = new UILang();
            templang = templang.getEngUILangData();
            LangSelectedText.text = LangSelectedText.text.Replace(templang.getlearnLang(), selectedLang.getlearnLang());
            ConvSelectedText.text = ConvSelectedText.text.Replace(templang.getlesson(), selectedLang.getlesson());

            templang = templang.getFraUILangData();
            LangSelectedText.text = LangSelectedText.text.Replace(templang.getlearnLang(), selectedLang.getlearnLang());
            ConvSelectedText.text = ConvSelectedText.text.Replace(templang.getlesson(), selectedLang.getlesson());

            SubmitText.text = selectedLang.gethomeSubmit();
        }
    }

    #endregion
    // deal with language selection
    #region LANGGAZE

    public void OnGazeEnterA()
    {
        GazeStartTime = Time.time;
        slider = LangA.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitA()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            BgA.color = Color.green;
            BgB.color = Color.red;
            BgC.color = Color.red;

            LangSelectedText.text = selectedLang.getlearnLang() + LangAText.text;
            SpRecLang = "en-GB";

            QuestionsSelected = a.getEnglishQuestions();
            locale = TTSManager.ENGLISH;
        }
    }

    public void OnGazeEnterB()
    {
        GazeStartTime = Time.time;
        slider = LangB.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitB()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            BgA.color = Color.red;
            BgB.color = Color.green;
            BgC.color = Color.red;

            LangSelectedText.text = selectedLang.getlearnLang() + LangBText.text;
            SpRecLang = "fr-FR";

            QuestionsSelected = a.getFrenchQuestions();
            locale = TTSManager.FRENCH;
        }
    }

    public void OnGazeEnterC()
    {
        GazeStartTime = Time.time;
        slider = LangC.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitC()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            BgA.color = Color.red;
            BgB.color = Color.red;
            BgC.color = Color.green;

            LangSelectedText.text = selectedLang.getlearnLang() + LangCText.text;
            SpRecLang = "de-DE";

            QuestionsSelected = a.getGermanQuestions();
            locale = TTSManager.GERMAN;
        }
    }

    #endregion

    // Deal with Conversation selection
    #region CONVGAZE

    public void OnGazeEnterConvA()
    {
        GazeStartTime = Time.time;
        slider = ConvA.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitConvA()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            ConBgA.color = Color.green;
            ConBgB.color = Color.red;
            ConBgC.color = Color.red;

            ConvSelectedText.text = selectedLang.getlesson() + ConvAText.text;
            Lesson = "Lesson1";
        }
    }

    public void OnGazeEnterConvB()
    {
        GazeStartTime = Time.time;
        slider = ConvB.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitConvB()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            ConBgA.color = Color.red;
            ConBgB.color = Color.green;
            ConBgC.color = Color.red;

            ConvSelectedText.text = selectedLang.getlesson() + ConvBText.text;
            Lesson = "Lesson2";
        }
    }

    public void OnGazeEnterConvC()
    {
        GazeStartTime = Time.time;
        slider = ConvC.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitConvC()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {
            ConBgA.color = Color.red;
            ConBgB.color = Color.red;
            ConBgC.color = Color.green;

            ConvSelectedText.text = selectedLang.getlesson() + ConvCText.text;
            Lesson = "Lesson3";
        }
    }

    public void OnGazeExitConv()
    {

    }

    #endregion

    #region SUBMITGAZE
    public void OnGazeEnterSubmit()
    {
        GazeStartTime = Time.time;
        slider = Submit.GetComponentInChildren<Slider>();
        InvokeRepeating("SliderEnter", 0f, 0.2f);
    }

    public void OnGazeExitSubmit()
    {
        SliderExit();
        GazeEndTime = Time.time;
        if (GazeEndTime - GazeStartTime > 2.0)
        {

            TTSCManager.selectedLang = selectedLang;
            SpeechManager.selectedLang = selectedLang;
            SpeechManager.updateUI();

            SpeechManager.setLang(SpRecLang);

            TTSCManager.UpdateQuestions(QuestionsSelected, locale);

            Left.enabled = false;
            Right.enabled = true;
            Center.enabled = false;
            MenuMessage.text = selectedLang.getinstruction1();
        }
    }

    #endregion

    public void SliderEnter()
    {
        sliderValue = sliderValue + 0.1f;
        slider.value = sliderValue;
    }

    public void SliderExit()
    {
        sliderValue = 0;
        slider.value = sliderValue;
        CancelInvoke("SliderEnter");
    }

    public void OnClick()
    {
        Debug.Log("Click Recived");
    }
}
