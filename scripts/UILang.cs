﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILang : ScriptableObject
{
    // define vars
    public string interfaceLang;
    public string learnLang;
    public string lesson;

    public string homeSubmit;
    public string resultSubmit;

    public string repeatQuestion;
    public string repeatResult;

    public string TTSLAng;
    public string SpRecLang;

    public string Listening;
    public string Record;
    public string SpeechResults;
    public string NextQuestion;
    public string TryAgain;
    public string instruction, instruction1, instruction2;

    public string Quit, QuitMessage, CompleteMessage;

    public TTSManager.Locale locale;

    // define constructor
    public UILang()
    {

    }


    public string getinstruction()
    {
        return instruction;
    }

    public string getinstruction1()
    {
        return instruction1;
    }

    public string getinstruction2()
    {
        return instruction2;
    }

    public string getinterfaceLang()
    {
        return interfaceLang;
    }

    public string getlearnLang()
    {
        return learnLang;
    }

    public string getlesson()
    {
        return lesson;
    }

    public string gethomeSubmit()
    {
        return homeSubmit;
    }

    public string getresultSubmit()
    {
        return resultSubmit;
    }

    public string getrepeatQuestion()
    {
        return repeatQuestion;
    }

    public string getrepeatResult()
    {
        return repeatResult;
    }

    public string getTTSLAng()
    {
        return TTSLAng;
    }

    public string getSpRecLang()
    {
        return SpRecLang;
    }

    public TTSManager.Locale getLocale()
    {
        return locale;
    }

    public string getListening()
    {
        return Listening;
    }
    public string getRecord()
    {
        return Record;
    }
    public string getSpeechResults()
    {
        return SpeechResults;
    }

    public string getNextQuestion()
    {
        return NextQuestion;
    }

    public string getTryAgain()
    {
        return TryAgain;
    }

    public string getQuit()
    {
        return Quit;
    }

    public string getQuitMessage()
    {
        return QuitMessage;
    }

    public string getCompleteMessage()
    {
        return CompleteMessage;
    }

    // Get UI Language Data
    public UILang getEngUILangData()
    {
        UILang a = new UILang();

        a.interfaceLang = "Interface Language Selected:";
        a.learnLang = "Language Selected:";
        a.lesson = "Lesson Selected:";
        a.homeSubmit = "Submit";
        a.resultSubmit = "Submit Selected:";
        a.repeatQuestion = "Repeat Question:";
        a.repeatResult = "Repeat Selected:";
        a.TTSLAng = "Eng";
        a.SpRecLang = "en-GB";
        a.locale = TTSManager.ENGLISH;
        a.Listening = "Listening";
        a.Record = "Record";
        a.SpeechResults = "Speech Results:";
        a.NextQuestion = "Next Question";
        a.TryAgain = "Incorrect, Try again";
        a.Quit = "Quit";
        a.QuitMessage = "Pick a new language/Lesson";
        a.CompleteMessage = "Well done: Pick a new language/Lesson";
        a.instruction = "To make a selection, look at a button until the bar turns completely green, then look away to confirm. \n" +
            "   1. Choose a 'User interface' language, the language you would like to learn and the lesson from the 1st, 2nd and 3rd columns respectively \n" +
            "   2. Select 'Submit' to proceed";
        a.instruction1 = "To make a selection, look at a button until the bar turns completely green, then look away to confirm. \n" +
            "Selecting the question will result in the character repeating the question to you. \n" +
            "   1. Choose the correct answer from the three options available (making a selection will allow you to hear the answer spoken to you)\n" +
            "   2. Select 'Submit Selected' to proceed";
        a.instruction2 = "To make a selection, look at a button until the bar turns completely green, then look away to confirm. \n" +
            "Selecting the answer will result in the character repeating the answer to you. - so that you can hear how it should sound  \n" +
            "   1.  Select 'Record' and repeat the answer after the beep, the second beep will indicate that the device is no longer listening.\n" +
            "   2.  If you got the statement correct, the button will turn green and a score (indicating the accuracy of your speech) will be displayed, otherwise the button will turn red and what we think you said will be displayed  \n" +
            "   3.  Select 'Next Question' when you are ready to proceed";


        return a;
    }

    public UILang getFraUILangData()
    {
        UILang a = new UILang();

        a.interfaceLang = "Langue d'interface sélectionnée:";
        a.learnLang = "Langue sélectionnée:";
        a.lesson = "Leçon choisie:";
        a.homeSubmit = "Soumettre";
        a.resultSubmit = "Soumettre sélectionné:";
        a.repeatQuestion = "Répétez la question:";
        a.repeatResult = "Répéter sélectionné:";
        a.TTSLAng = "Fra";
        a.SpRecLang = "fr-FR";
        a.locale = TTSManager.FRENCH;
        a.Listening = "Écoute";
        a.Record = "Record";
        a.SpeechResults = "Résultats de la parole:";
        a.NextQuestion = "Question suivante";
        a.TryAgain = "Faux. Essayez à nouveau";
        a.Quit = "Quitter";
        a.QuitMessage = "Choisissez une nouvelle langue / Leçon";
        a.CompleteMessage = "Bien fait: Choisissez une nouvelle langue / Leçon";
        a.instruction = "Pour faire une sélection, regardez un bouton jusqu'à ce que la barre soit complètement verte, puis regardez loin pour confirmer. \n" +
            "   1. Choisissez une langue 'Interface utilisateur', la langue que vous souhaitez apprendre et la leçon des 1ères, 2ème et 3ème colonnes respectivement \n" +
            "   2. Sélectionnez 'Soumettre' pour continuer";
        a.instruction1 = " Pour faire une sélection, regardez un bouton jusqu'à ce que la barre soit complètement verte, puis regardez loin pour confirmer. \n" +
            " En sélectionnant la question, le personnage vous répétera la question.\n" +
            "   1. Choisissez la réponse correcte à partir des trois options disponibles (faire une sélection vous permettra d'entendre la réponse qui vous a parlé) \n" +
            "   2. Sélectionnez 'Soumettre sélectionné' pour continuer";
        a.instruction2 = " Pour faire une sélection, regardez un bouton jusqu'à ce que la barre soit complètement verte, puis regardez loin pour confirmer.\n" +
            " En sélectionnant la réponse, le personnage vous répétera la réponse. - afin que vous puissiez entendre comment cela devrait paraître  \n" +
            "   1.  Sélectionnez 'Enregistrer' et répétez la réponse après le bip, le deuxième bip indiquera que l'appareil n'écoute plus. \n" +
            "   2.  Si vous avez déclaré correctement la déclaration, le bouton devient vert et un score (indiquant la précision de votre discours) s'affiche, sinon le bouton devient rouge et ce que nous pensons que vous avez dit sera affiché  \n" +
            "   3.  Sélectionnez 'Question suivante' lorsque vous êtes prêt à continuer ";


        return a;
    }

    public UILang getGerUILangData()
    {
        UILang a = new UILang();

        a.interfaceLang = "Schnittstelle Sprache ausgewählt:";
        a.learnLang = "Sprache Ausgewählt:";
        a.lesson = "Lektion Ausgewählt:";
        a.homeSubmit = "Einreichen";
        a.resultSubmit = "Submit Ausgewählt:";
        a.repeatQuestion = "Wiederholen Frage:";
        a.repeatResult = "Wiederholen Ausgewählt:";
        a.TTSLAng = "Ger";
        a.SpRecLang = "de-DE";
        a.locale = TTSManager.GERMAN;
        a.Listening = "Hören";
        a.Record = "Aufzeichnung";
        a.SpeechResults = "Sprachergebnisse:";
        a.NextQuestion = "Nächste Frage";
        a.TryAgain = "Falsch. Versuche es noch einmal";
        a.Quit = "Verlassen";
        a.QuitMessage = "Wähle eine neue Sprache / Lektion";
        a.CompleteMessage = "Gut gemacht: Wähle eine neue Sprache / Lektion";
        a.instruction = "Um eine Auswahl zu treffen, schau auf einen Knopf, bis die Bar ganz grün wird und dann wegschauen, um zu bestätigen. \n" +
            "   1. Wählen Sie eine Sprache 'Benutzeroberfläche', die Sprache, die Sie lernen möchten, und die Lektion aus der 1., 2. und 3. Spalte \n" +
            "   2. Wählen Sie 'Submit', um fortzufahren";
        a.instruction1 = "Um eine Auswahl zu treffen, schau auf einen Knopf, bis die Bar ganz grün wird und dann wegschauen, um zu bestätigen. \n" +
            "Wenn Sie die Frage auswählen, wird das Zeichen wiederholt, das Ihnen die Frage wiederholt. \n" +
            "   1. Wählen Sie die richtige Antwort aus den drei verfügbaren Optionen (eine Auswahl ermöglicht es Ihnen, die Antwort zu hören, die mit Ihnen gesprochen wird) \n" +
            "   2. Wählen Sie 'Submit Selected', um fortzufahren";
        a.instruction2 = "Um eine Auswahl zu treffen, schau auf einen Knopf, bis die Bar ganz grün wird und dann wegschauen, um zu bestätigen. \n" +
            "Die Auswahl der Antwort wird dazu führen, dass das Zeichen die Antwort auf Sie wiederholt. - damit du hören kannst, wie es klingen soll  \n" +
            "   1.  Wählen Sie 'Record' und wiederholen Sie die Antwort nach dem Signalton, der zweite Signalton zeigt an, dass das Gerät nicht mehr hört. \n" +
            "   2.  Wenn du die Aussage richtig gemacht hast, wird die Schaltfläche grün und eine Punktzahl (die die Genauigkeit Ihrer Sprache anzeigt) angezeigt wird, sonst wird die Taste rot und was wir denken, dass sie angezeigt werden  \n" +
            "   3.  Wählen Sie 'Nächste Frage', wenn Sie bereit sind, fortzufahren";


        return a;
    }

}
